/* ------------------------- locusamp~ ---------------------------------------- */
/*                                                                              */
/* Tilde object primarily written to receive an Ogg Vorbis/Opus/MP3/AAC stream  */
/* from an IceCast or ShoutCast server. Can be also use to read and work with   */
/* any local or remote files, in many audio format.                             */
/* Written by Grégoire Lauvin <greglauvin@gmail.com> and Stéphane Cousot        */
/* <stef@ubaa.net>                                                              */
/* Get source at http://locusonus.org/pd/                                       */
/*                                                                              */
/* Based on PureData by Miller Puckette and others.                             */
/* Uses the FFmpeg library which can be found at http://www.ffmpeg.com/         */
/*                                                                              */
/* ---------------------------------------------------------------------------- */
/*                                                                              */
/* FFmpeg based multi format streaming client.                                  */
/* Copyright (C) 2017 Grégoire Lauvin and Stéphane Cousot.                      */
/*                                                                              */
/* This program is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by         */
/* the Free Software Foundation, either version 3 of the License, or            */
/* (at your option) any later version.                                          */
/*                                                                              */
/* This program is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/* GNU General Public License for more details.                                 */
/*                                                                              */
/* You should have received a copy of the GNU General Public License            */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>.        */
/* ---------------------------------------------------------------------------- */

#include "m_pd.h"
#include "locusonus.h"

#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"

#include <pthread.h>
#include <unistd.h>


#define LOCUSAMP_VERSION "1.0"

// state
#define STATE_DISCONNECTED		0
#define STATE_CONNECTING		1
#define STATE_ABORTED			2
#define STATE_CONNECTED 		3

// message type
#define TAG_ERROR		"error"
#define TAG_WARNING		"warning"
#define TAG_METADATA	"metadata"

// audio
#define DEF_OUTPUT_CHANNELS		2			// number of channels by default
#define MAX_OUTPUT_CHANNELS		64			// maximum number of channels
#define MIN_BUFFER_SIZE			2048        // minimum circular buffer size

// thread
#define DEF_DECODER_INTERVAL    5000        // default decoder thread interval in microseconds


////////////////////////////////////////////////////////////////////////////////////////////////////


static char *locusamp_message = "live audio stream client v%s, by Grégoire Lauvin and Stéphane Cousot, 2017 Locus Sonus reseach lab";

/* -------------------------------------- locusamp~ --------------------------------------------- */

static t_class *locusamp_class;

typedef struct _buffer
{
    t_float * data;  // audio data
    int32_t head;    // write pointer
    int32_t tail;    // read pointer
    t_int have;      // available data
    t_int size;	     // buffer capacity
}
t_locusamp_buffer;

typedef struct _locusamp
{
	t_object x_obj;
    
    t_locusamp_buffer buffer;	// internal circular buffer
	t_locusamp_buffer samples;	// decoded and resampled incoming audio data

    t_outlet **sig_outlet;      // object audio outlets
	t_sample **sig_vec;         // output audio data / signal vectors
	t_int      sig_size;		// signal vector size (i.e. PD block size)
	t_int      sig_ch;          // number of audio outlets
	int64_t    sig_layout;      // channel layout for the corresponding number of channels
	t_float    sig_sr;          // object i.e. PD sample rate

	int32_t state;				// stream status: disconnected, connecting, connected
	t_outlet *o_connected;      // connected outlet
	t_outlet *o_anything;		// output error, metadata, message, audio configuration



	const char *src;			// the stream URL or file path
    
	pthread_t t_open;			// loader thread
	pthread_t t_dec;			// decoder thread
    t_int     t_dec_ms;         // decoder thread microsecond intervals
    
    int8_t running;             // true while listening to stream
    int8_t starving;            // true while request samples
    int8_t eof;                 // end of file

	AVFormatContext *format;	// audio format context
	AVCodecContext  *codec;		// audio codec context
	SwrContext      *swr;		// resampler context
	AVPacket		*packet;	// compressed audio data
	AVFrame         *frame;		// decoded (raw) audio data

}
t_locusamp;


typedef struct _interrupt_context
{
    const char *src;		// libav source
    AVFormatContext *ctx;	// libav context
}
t_locusamp_interrupt_context;


// libav object pointer
static t_locusamp *libav_x;


// post log with 'verbose' level
static void logv( const char *fmt, ... )
{
	char buffer[MAXPDSTRING];

	va_list arg;
	va_start(arg,fmt);
	vsprintf(buffer,fmt,arg);
	va_end(arg);

	//logpost( NULL, 3, "locusamp~: %s", buffer );
    post( "locusamp~: %s", buffer );
}

static void string2atom( t_atom *a, char* s, size_t slen )
{
	char *buffer = getbytes( (slen+1)*sizeof(char) );
	char *endptr[1];
	t_float f;

	strncpy( buffer, s, slen );
	buffer[slen]=0;

	f = strtof( buffer, endptr );
	if ( buffer+slen!=*endptr ) SETSYMBOL( a, gensym(buffer) ); // strtof() failed, we have a symbol
	else SETFLOAT( a, f );

	freebytes( buffer, (slen+1)*sizeof(char) );
}




/////// OUTLET /////////////////////////////////////////////////////////////////////////////////////

static void locusamp_dump( t_locusamp *x, char* tag, char* msg )
{
	if ( strlen(msg)==0 ) return;

	int tagged = tag==NULL || strlen(tag)==0 ? 0 : 1;
	size_t len = tagged ? strlen(tag)+strlen(msg) : strlen(msg);
	t_atom *av = getbytes( len*sizeof(t_atom) );

	if ( !tagged ) string2atom( av, msg, strlen(msg) );
	else {
		string2atom( av+0, tag, strlen(tag) );
		string2atom( av+1, msg, strlen(msg) );
	}

	outlet_anything( x->o_anything, av->a_w.w_symbol, tagged, av+tagged );
	freebytes( av, len*sizeof(t_atom) );
}

static void locusamp_info( t_locusamp *x )
{
    // stream info
	if ( x->codec ) {
		char buffer[MAXPDSTRING];

		sprintf( buffer, "%s", x->codec->codec->name );
		locusamp_dump( x, "codec", buffer );
		sprintf( buffer, "%i", x->codec->sample_rate );
		locusamp_dump( x, "samplerate", buffer );
		sprintf( buffer, "%i", x->codec->channels );
		locusamp_dump( x, "channel", buffer );
		sprintf( buffer, "%i", (int)x->codec->bit_rate );
		locusamp_dump( x, "bitrate", buffer );
		locusamp_dump( x, "sampleformat", (char*)av_get_sample_fmt_name(x->codec->sample_fmt) );
	}
    else {
        locusamp_dump( x, "codec", "symbol" );
        locusamp_dump( x, "samplerate", "0" );
        locusamp_dump( x, "channel", "0" );
        locusamp_dump( x, "bitrate", "0" );
        locusamp_dump( x, "sampleformat", "symbol" );
    }

    // metadata
	if ( x->format ) {
		char meta[MAXPDSTRING];
		AVDictionary * d = x->format->metadata;
		AVDictionaryEntry *t = NULL;
		
		while( (t=av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX))!=0 ) {
			sprintf( meta, "%s: %s", t->key, t->value );
			locusamp_dump( x, TAG_METADATA, meta );
		}
	}
}

static void locusamp_state( t_locusamp *x, int state )
{
    int pval = (x->state==STATE_CONNECTED);
    int nval = (state==STATE_CONNECTED);
    
	x->state = state;
	if ( pval!=nval ) outlet_float( x->o_connected, nval );
}




/////// LIBAV //////////////////////////////////////////////////////////////////////////////////////

static void libav_log_callback( void *avcl, int level, const char *fmt, va_list vl )
{
    if ( level>=0 && level<=av_log_get_level() ) {
    
        char buffer[MAXPDSTRING];
        vsprintf( buffer, fmt, vl );
        if ( buffer[strlen(buffer)-1]=='\n' )  buffer[strlen(buffer)-1]=0;
        
        if ( avcl ) logv( "%s: %s", av_default_item_name(avcl), buffer );
        if ( libav_x ) {
            locusamp_dump( libav_x, NULL, buffer );
            libav_x = NULL;
        }
    }
};




/////// AUDIO //////////////////////////////////////////////////////////////////////////////////////

#define MIN(a,b) (a>b?b:a)
static void locusamp_disconnect( t_locusamp *x ); // declaration

static int locusamp_decode( t_locusamp *x )
{

	int i, len, err = 0;
    libav_x = x;

	// read the next frame of a stream
	av_packet_unref( x->packet );
	if ( (err=av_read_frame(x->format,x->packet))<0 ) goto failed;// fatal error or end file
	
		
	av_frame_unref( x->frame );
			
	// send raw datato the decoder and 
	// get the decoded data from the decoder
	if ( (err=avcodec_send_packet(x->codec,x->packet))<0 ) goto done;
	if ( (err=avcodec_receive_frame(x->codec,x->frame))<0 ) goto done;
	

	// allocate resampler context
	if ( x->swr== NULL ) {
		int64_t in_layout = x->frame->channel_layout ?: av_get_default_channel_layout(x->codec->channels);
		enum AVSampleFormat in_sample_fmt = x->frame->format;
		x->swr = swr_alloc_set_opts(
			NULL, x->sig_layout, AV_SAMPLE_FMT_FLT, x->sig_sr,
			in_layout, in_sample_fmt, x->frame->sample_rate, 0, NULL
		);
		if ( (err=swr_init(x->swr))<0 ) goto failed; // failed to initialize the resampling 
	}
	
	// resample to desired rate and store in buffer
	//uint8_t *out = (uint8_t*)(x->buffer.data);
    uint8_t *out = (uint8_t*)(x->samples.data);
	int nsamples = swr_convert( x->swr, (uint8_t**)&out, x->samples.size/x->sig_ch, (const uint8_t**)x->frame->extended_data, x->frame->nb_samples );
    

    // copy samples
    for( i=0, len=(nsamples*x->sig_ch); i<len; i++ ) {
        
        x->buffer.data[ x->buffer.head++ ] = x->samples.data[i];
        if ( x->buffer.head>=x->buffer.size ) x->buffer.head = 0;
        
        // warn before override data
        // TODO: think about modulo to warn each cycle without resetting the counter ?
        x->buffer.have++;
        if ( x->buffer.have>=x->buffer.size ) {
            locusamp_dump( x, TAG_WARNING, "Buffer overrun" );
            x->buffer.have = 0;
        }
    }
    

done:

	if ( err<0 ) locusamp_dump( x, TAG_WARNING, av_err2str(err) );
	return (err>=0);

failed:

    if ( err!=AVERROR_EOF ) {
        locusamp_dump( x, TAG_ERROR, av_err2str(err) );
        locusamp_disconnect(x);
    }
    else x->eof = 1;
    
	return (0);
}

static void *locusamp_decode_async( void *arg )
{
    t_locusamp *x = (t_locusamp*)arg;
    
    //struct timespec wait = {0,x->t_dec_ms};
    
    x->buffer.head = 0;
    x->buffer.tail = 0;
    x->buffer.have = 0;
    
    x->starving = 1;
    x->running  = 1;
    x->eof = 0;
    
    t_int max = x->buffer.size; // limit to wait a bit to minimize buffer overrun
    while( x->running ) {
        if ( x->state==STATE_CONNECTED && !x->eof && x->buffer.have<max ) locusamp_decode(x);
        //nanosleep( &wait, NULL );
        usleep( x->t_dec_ms );
    }
    
	pthread_exit(NULL);
	return NULL;
}

t_int *locusamp_perform( t_int *w )
{
    t_locusamp *x = (t_locusamp *)(w[1]);
    int i, c;
    
    if ( x->state==STATE_CONNECTED ) {
        
        t_int samples = x->buffer.have / x->sig_ch; // available samples
        
        // copy
        if ( samples>=x->sig_size ) {
            
            for( i=0; i<x->sig_size; i++ ) {
                for( c=0; c<x->sig_ch; c++ ) {
                    *(x->sig_vec[c]+i) = *(x->buffer.data+x->buffer.tail);
                    if ( (++x->buffer.tail)>=x->buffer.size ) x->buffer.tail = 0;
                    x->buffer.have--;
                }
            }
            
            // have enough samples
            x->starving = 0;
            goto done;
        }
        
        // wait or disconnect (EOF)
        if ( !x->starving ) {
            if ( x->eof ) {
                locusamp_dump( x, TAG_ERROR, av_err2str(AVERROR_EOF) );
                locusamp_disconnect(x);
            }
            else locusamp_dump( x, TAG_WARNING, "Missing samples" );
            x->starving = 1;
        }
    }
    
    // idle
    for( c=0; c<x->sig_ch; c++ )
        memset( x->sig_vec[c], 0, x->sig_size*sizeof(float) );
    
done:
    return (w+2);
}


static void locusamp_dsp( t_locusamp *x, t_signal **sp )
{
	x->sig_size = sp[0]->s_n;
	for( int i=0; i<x->sig_ch; i++ )
		x->sig_vec[i] = sp[i]->s_vec;

	dsp_add( locusamp_perform, 1, x );
}




/////// LOADER /////////////////////////////////////////////////////////////////////////////////////

// abort blocking operation
static t_locusamp_interrupt_context interrupt_context;
static int locusamp_interrupt_callback( void *ctx )
{
	if ( interrupt_context.ctx==(AVFormatContext*)ctx ) {
        t_locusamp *x = (t_locusamp *)interrupt_context.ctx->opaque;
        if ( x->state!=STATE_ABORTED ) {
            logv( "aborting operation for %s", interrupt_context.src );
            //locusamp_dump( x, TAG_WARNING, "aborting operation" ); // TODO: DO WE NEED TO DUMP MESSSAGE ?
            locusamp_state( x, STATE_ABORTED );
        }
        else interrupt_context.ctx = NULL;
        return 1;
	}
	return 0;
}

static void locusamp_disconnect( t_locusamp *x )
{
    
    // aborting loading process
    if ( x->state==STATE_CONNECTING ) {
        interrupt_context.ctx = x->format;
        interrupt_context.src = x->src;
    }
    if ( x->state<STATE_CONNECTED ) return;
    
    // waiting for thread
    if ( x->running ) {
        x->running = 0;
        pthread_join( x->t_dec, NULL );
    }
	
	if ( x->swr ) swr_free( &x->swr );
	if ( x->frame ) av_frame_free( &x->frame );
	if ( x->codec ) avcodec_close( x->codec );
	if ( x->format ) avformat_close_input( &x->format );
	
	x->format   = NULL;
	x->codec    = NULL;
	x->swr      = NULL;
	x->frame    = NULL;

    locusamp_info( x );
    locusamp_state( x, STATE_DISCONNECTED );
    
	logv( "disconnected" );
}

static void locusamp_open( t_locusamp *x )
{
	
	int res = 0;
    libav_x = x;

    logv( "open %s", x->src );

    
    x->format = avformat_alloc_context();
    x->format->interrupt_callback.callback = locusamp_interrupt_callback;
    x->format->interrupt_callback.opaque = x->format;
    x->format->opaque = x;
    
    res = avformat_open_input( &x->format, x->src, NULL, NULL );
	if ( res!=0 ) {
        if ( x->state==STATE_ABORTED ) {
            locusamp_dump( x, TAG_WARNING, "aborted operation" ); // TODO: DO WE NEED TO DUMP MESSSAGE ?
            // locusamp_dump( x, TAG_ERROR, av_err2str(res) ); // TODO: DO WE NEED TO DUMP MESSSAGE ?
            return;
        }
        else locusamp_dump( x, NULL, "unable to open input source" );
		goto failed;
	}
    
	res = avformat_find_stream_info( x->format, NULL );
	if ( res<0 ) {
		locusamp_dump( x, NULL, "unable to find stream info" );
		goto failed;
	}


	// check for audio stream
	int stream = -1;
	for( unsigned int i=0; i<x->format->nb_streams; i++ ) {
		if ( x->format->streams[i]->codecpar->codec_type==AVMEDIA_TYPE_AUDIO && stream==-1 ) {
			stream = i;
		}
	}
	if ( stream==-1 ) {
		locusamp_dump( x, TAG_ERROR, "unable to find audio stream" );
		goto failed;
	}
	
	AVCodec *codec = avcodec_find_decoder( x->format->streams[stream]->codecpar->codec_id );
	if ( codec==NULL ) {
		locusamp_dump( x, TAG_ERROR, "unsupported audio codec" );
		goto failed;
	}
	x->codec=avcodec_alloc_context3( codec );
	avcodec_parameters_to_context( x->codec, x->format->streams[stream]->codecpar );
	res = avcodec_open2( x->codec, codec, NULL );
	if ( res<0 ) {
		locusamp_dump( x, TAG_ERROR, "unsupported audio codec" );
		goto failed;
	}
	
	x->frame = av_frame_alloc();
	if ( x->frame==NULL ) {
		locusamp_dump( x, TAG_ERROR, "unable to allocate audio frame" );
		goto failed;
	}
    
    
	x->packet = (AVPacket *)av_malloc(sizeof(AVPacket));
	av_init_packet(x->packet);

	locusamp_info( x );
	locusamp_state( x, STATE_CONNECTED );
    
    // start decoder thread
    int err = pthread_create( &x->t_dec, NULL, locusamp_decode_async, x );
    if ( err ) {
        char msg[MAXPDSTRING];
        sprintf( msg, "unable to separate decoder from main thread: %s", strerror(err) );
        locusamp_dump( x, TAG_ERROR, msg );
        return;
    }
    
	logv( "connected to %s", x->src );
	return;


failed:
    
	if ( res ) locusamp_dump( x, TAG_ERROR, av_err2str(res) );
	logv( "unable to open %s", x->src );
	locusamp_state( x, STATE_DISCONNECTED );
	return;

}

static void *locusamp_open_async( void *arg )
{
    t_locusamp *x = (t_locusamp*)arg;
	if ( x->state==STATE_CONNECTING ) locusamp_open( x );
	pthread_exit(NULL);
	return NULL;
}

static void locusamp_connect( t_locusamp *x, t_symbol *url )
{
    
    // close previous stream
	if ( x->format ) locusamp_disconnect(x);

	x->src = url->s_name;
	locusamp_state( x, STATE_CONNECTING );

	// open url asynchronous
	int err = pthread_create( &x->t_open, NULL, locusamp_open_async, x );
	if ( err ) {
		char msg[MAXPDSTRING];
		sprintf( msg, "unable to loader: %s", strerror(err) );
		locusamp_dump( x, TAG_ERROR, msg );
		return;
	}
}




/////// SETUP //////////////////////////////////////////////////////////////////////////////////////

static void locusamp_free( t_locusamp *x )
{
	locusamp_disconnect(x);
    
	for( int i=0; i<x->sig_ch; i++ ) 
		outlet_free( x->sig_outlet[i] );

	outlet_free( x->o_anything );
	outlet_free( x->o_connected );
    
    freebytes( x->buffer.data, x->buffer.size*sizeof(t_float) );
    freebytes( x->samples.data, x->samples.size*sizeof(t_float) );
    freebytes( x->sig_vec, x->sig_ch*sizeof(t_sample *) );
    freebytes( x->sig_outlet, x->sig_ch*sizeof(t_outlet *) );
}

static void *locusamp_new( t_floatarg arg1, t_floatarg arg2, t_floatarg arg3, t_floatarg arg4 )
{

	// Initialize libavformat
	// and network components
	av_register_all();
	avformat_network_init();

    
	// create and init object
	t_locusamp *x 	= (t_locusamp*)pd_new( locusamp_class );
    x->sig_size     = sys_getblksize();
	x->sig_sr       = sys_getsr();
	x->sig_ch       = arg1<1 ? DEF_OUTPUT_CHANNELS : ( arg1>MAX_OUTPUT_CHANNELS ? MAX_OUTPUT_CHANNELS : arg1 );
	x->sig_layout   = av_get_default_channel_layout( x->sig_ch );
    x->buffer.size  = arg2 && arg2>MIN_BUFFER_SIZE ? arg2 : MIN_BUFFER_SIZE;
    x->buffer.head  = 0;
    x->buffer.tail  = 0;
    x->buffer.have  = 0;
    x->samples.size = arg3 && arg3>x->sig_size ? arg3 : x->sig_size;
    x->state 	    = STATE_DISCONNECTED;
	x->format       = NULL;
	x->codec        = NULL;
	x->swr          = NULL;
	x->frame        = NULL;
    x->packet       = NULL;
    x->t_open       = 0;
    x->t_dec        = 0;
    x->t_dec_ms     = arg4 ? arg4*1000 : DEF_DECODER_INTERVAL;
    x->running      = 0;
    x->starving     = 0;
    
    

	// define buffers size (in bytes)
    x->buffer.size  *= x->sig_ch * 1024;
    x->samples.size *= x->sig_ch * 1204;
    
	// allocate buffers
    x->samples.data = getbytes( x->samples.size*sizeof(t_float) );
	x->buffer.data  = getbytes( x->buffer.size*sizeof(t_float) );
	x->sig_vec = (t_sample **)getbytes( x->sig_ch*sizeof(t_sample *) );
    if ( !x->samples.data || !x->buffer.data || !x->sig_vec ) return (0);
    

	// create output channels (dsp outlet)
    x->sig_outlet = (t_outlet**)getbytes( x->sig_ch*sizeof(t_outlet*) );
	for( int i=0; i<x->sig_ch; i++ )
		x->sig_outlet[i] = outlet_new( &x->x_obj, &s_signal );

	// create status outlets
	x->o_anything = outlet_new( &x->x_obj, 0 );
	x->o_connected = outlet_new( &x->x_obj, &s_float );

	return (void *)x;
}

void locusamp_tilde_setup( void )
{

    logv( locusamp_message, LOCUSAMP_VERSION );
	logv( "using Libavcodec version %s", AV_STRINGIFY(LIBAVCODEC_VERSION) );

	// rouet dcoder message
	//av_log_set_level( AV_LOG_VERBOSE );
	av_log_set_callback( libav_log_callback );
    libav_x = NULL;


	// create class object
	locusamp_class = class_new(gensym("locusamp~"), 
		(t_newmethod)locusamp_new, (t_method)locusamp_free, sizeof(t_locusamp), 
		CLASS_DEFAULT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, 0
	);

	class_addmethod( locusamp_class, (t_method)locusamp_dsp, gensym("dsp"), 0 );
	class_addmethod( locusamp_class, (t_method)locusamp_connect, gensym("connect"), A_SYMBOL, 0 );
	class_addmethod( locusamp_class, (t_method)locusamp_disconnect, gensym("disconnect"), 0 );
	class_addmethod( locusamp_class, (t_method)locusamp_info, gensym("bang"), 0 );
}

