#!/bin/sh
#
# Compile FFmpeg for the pd-locusonus external libray under Arch Linux ARM
# Update your system first (sudo pacman -Syu)
# Require gcc, make, curl, pkg-config, git and yasm (sudo pacman -S make gcc pkg-config curl git yasm)
#


# stop on errors
set -e


TARGET="$(cd "$(dirname "$0")" && pwd)"
SOURCE="${TARGET}/src"

 
mkdir -p ${TARGET}
mkdir -p ${SOURCE}

# add the target bin/ directory to the $PATH 
export PATH=${TARGET}/bin:$PATH




# ------------------------------------------------------------------------------
# LAME
# http://lame.sourceforge.net
# ------------------------------------------------------------------------------

LAME_VERSION=3.100
LAME_SRC_DIR="lame-${LAME_VERSION}"
LAME_ARCHIVE="${LAME_SRC_DIR}.tar.gz"
LAME_SRC_URL="https://downloads.sourceforge.net/project/lame/lame/${LAME_VERSION}/${LAME_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libmp3lame.a ]; then

	echo "--- compile LAME library"

	cd ${SOURCE}
	if [ ! -f ${LAME_ARCHIVE} ]; then curl -L -O ${LAME_SRC_URL}; fi
	if [ ! -d ${LAME_SRC_DIR} ]; then tar xzpf ${LAME_ARCHIVE}; fi

	cd ${LAME_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install
else
	echo "--- nothing to do with LAME library"
fi



# ------------------------------------------------------------------------------
# OGG
# https://www.xiph.org/
# ------------------------------------------------------------------------------

OGG_VERSION=1.3.4
OGG_SRC_DIR="libogg-${OGG_VERSION}"
OGG_ARCHIVE="${OGG_SRC_DIR}.tar.gz"
OGG_SRC_URL="http://downloads.xiph.org/releases/ogg/${OGG_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libogg.a ]; then

	echo "--- compile Ogg library"

	cd ${SOURCE}
	if [ ! -f ${OGG_ARCHIVE} ]; then curl -L -O ${OGG_SRC_URL}; fi
	if [ ! -d ${OGG_SRC_DIR} ]; then tar xzpf ${OGG_ARCHIVE}; fi

	cd ${OGG_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install
else
	echo "--- nothing to do with Ogg library"
fi


# ------------------------------------------------------------------------------
# VORBIS
# https://www.xiph.org/
# ------------------------------------------------------------------------------

VORBIS_VERSION=1.3.6
VORBIS_SRC_DIR="libvorbis-${VORBIS_VERSION}"
VORBIS_ARCHIVE="${VORBIS_SRC_DIR}.tar.gz"
VORBIS_SRC_URL="http://downloads.xiph.org/releases/vorbis/${VORBIS_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libvorbis.a ]; then

	echo "--- compile Vorbis libraries"

	cd ${SOURCE}
	if [ ! -f ${VORBIS_ARCHIVE} ]; then curl -L -O ${VORBIS_SRC_URL}; fi
	if [ ! -d ${VORBIS_SRC_DIR} ]; then tar xzpf ${VORBIS_ARCHIVE}; fi

	cd ${VORBIS_SRC_DIR}
	./configure --prefix=${TARGET} \
				--with-ogg-libraries=${TARGET}/lib \
				--with-ogg-includes=${TARGET}/include/ \
				--enable-static --disable-shared && make -j 4 && make install
else
	echo "--- nothing to do with Vorbis libraries"
fi



# ------------------------------------------------------------------------------
# OPUS
# http://www.opus-codec.org
# ------------------------------------------------------------------------------

OPUS_VERSION=1.3.1
OPUS_SRC_DIR="opus-${OPUS_VERSION}"
OPUS_ARCHIVE="${OPUS_SRC_DIR}.tar.gz"
OPUS_SRC_URL="http://downloads.xiph.org/releases/opus/${OPUS_ARCHIVE}"

if [ ! -f ${TARGET}/lib/libopus.a ]; then

	echo "--- compile Opus library"

	cd ${SOURCE}
	if [ ! -f ${OPUS_ARCHIVE} ]; then curl -L -O ${OPUS_SRC_URL}; fi
	if [ ! -d ${OPUS_SRC_DIR} ]; then tar xzpf ${OPUS_ARCHIVE}; fi

	cd ${OPUS_SRC_DIR}
	./configure --prefix=${TARGET} --disable-shared --enable-static && make -j 4 && make install
else
	echo "--- nothing to do with Opus libraries"
fi



# ------------------------------------------------------------------------------
# FFMPEG
# http://www.ffmpeg.org
# ------------------------------------------------------------------------------

FFMPEG_SRC_DIR="ffmpeg"
FFMPEG_OPTIONS="--enable-gpl --enable-pthreads --enable-version3 --enable-static --disable-shared \
               --enable-libmp3lame --enable-libopus --enable-libvorbis \
               --enable-avfilter --enable-filters --enable-runtime-cpudetect \
               --disable-ffplay --disable-ffmpeg --disable-ffprobe --disable-ffserver"


if [ ! -f ${TARGET}/lib/libavcodec.a ]; then

	echo "--- compile FFmpeg libraries"

	cd ${SOURCE}
	if [ ! -d ${FFMPEG_SRC_DIR} ]; then git clone git://source.ffmpeg.org/ffmpeg.git; fi

	cd ${FFMPEG_SRC_DIR}
	./configure --prefix="${TARGET}" ${FFMPEG_OPTIONS} && make -j 4 && make install
else
	echo "--- nothing to do with FFmpeg libraries"
fi

